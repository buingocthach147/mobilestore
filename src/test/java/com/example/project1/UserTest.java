package com.example.project1;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.TestDatabaseAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
public class UserTest {
    @Autowired
    private UserRepository repository;
    private TestEntityManager testEntityManager;
    @Test
    public void testCreateUser(){
        User user = new User();
        user.setUsername("admin");
        user.setPassword("admin");
        user.setTypeaccount(1);
        User saveUser = repository.save(user);
        User exitUser = testEntityManager.find(User.class,saveUser.getId());
        assertThat(exitUser.getUsername()).isEqualTo(user.getUsername());
    }
}
