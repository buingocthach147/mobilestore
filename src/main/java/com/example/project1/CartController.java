package com.example.project1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class CartController {
    @Autowired private CartService cartService;

    @GetMapping("/carts")
    public  String showProductList(Model model){
        List<Cart> listCart = cartService.listAllCart();
        model.addAttribute("list",listCart);

        return "carts";
    }

    @GetMapping("/cart/add/{id}")
    public  String showDetailsProduct(@PathVariable("id") Integer id, Model model){

        cartService.AddtoCart(id);
        return "redirect:/";
    }



}

