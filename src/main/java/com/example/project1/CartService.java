package com.example.project1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class CartService {
    @Autowired CarRepository cartRepo;

    @Autowired ProductRepository productRepository;

    public List<Cart> listAllCart(){
        return (List<Cart>) cartRepo.findAll();

    }

    public void AddtoCart(Integer id){
        Cart cart = new Cart();
        Optional<Product> product1 = productRepository.findById(id);
        Product product = product1.get();
        cart.setName(product.getPname());
        cart.setPrice(product.getPrice());
        cart.setQuantity(product.getStock());
        cartRepo.save(cart);
    }
}
